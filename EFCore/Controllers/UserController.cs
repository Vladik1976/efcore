﻿using Microsoft.AspNetCore.Mvc;
using EFCore.Common.DTOs.Project;
using EFCore.Common.DTOs.Task;
using EFCore.Common.DTOs.User;
using EFCore.BLL.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFCore.WebAPI.Controllers
{
    [Route("api/users/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<Dictionary<ProjectDTO, int>> GetProjects(int id)
        {
            var projects = await _userService.GetProjects(id);
           
            return projects;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<List<TaskDTO>> GetTasks(int id)
        {

            var tasks= await _userService.GetTasks(id);

            return tasks;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<List<Tuple<int, string>>> GetFinishedTasks(int id)
        {
            return await _userService.GetFinishedTasks(id);
        }

        [HttpGet]
        public async Task<List<UserTasksDTO>> GetUsersWithTasks()
        {
            var users= await _userService.GetUsersWithTasks();
            

            return users;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<UserDetailsDTO> GetUserDetails(int id)
        {
            var user = await _userService.GetUserDetails(id);

            return user;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<UserDTO> GetUser(int id)
        {
            var user = await _userService.GetUser(id);

            return user;
        }

        [HttpGet]
        public async Task<IEnumerable<UserDTO>> Get()
        {
            return await _userService.GetUsers();
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Create([FromBody] UserCreateDTO dto)
        {
            return Ok(await _userService.CreateUser(dto));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UserDTO dto)
        {
            await _userService.UpdateUser(dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
