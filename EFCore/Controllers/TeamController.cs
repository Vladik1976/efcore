﻿using Microsoft.AspNetCore.Mvc;
using EFCore.BLL.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCore.Common.DTOs.Team;

namespace EFCore.WebAPI.Controllers
{
    [Route("api/teams/[action]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;
        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<IEnumerable<TeamDTO>> Get()
        {
            return await _teamService.GetTeams();
        }

        [HttpGet]
        public async Task<List<TeamLimitedDTO>> GetTeamsLimited()
        {
            var teams= _teamService.GetTeamsLimited();
            return await teams;
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Create([FromBody] TeamCreateDTO dto)
        {
            return Ok(await _teamService.CreateTeam(dto));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TeamDTO dto)
        {
            await _teamService.UpdateTeam(dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _teamService.DeleteTeam(id);
            return NoContent();
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<TeamDTO> GetTeam(int id)
        {
            var team = await _teamService.GetTeam(id);

            return team;
        }

    }
}
