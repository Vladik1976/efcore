﻿using EFCore.BLL.Services;
using EFCore.Common.DTOs.Task;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.WebAPI.Controllers
{
    [Route("api/tasks/[action]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskDTO>> Get()
        {
            return await _taskService.GetTasks();
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Create([FromBody] TaskCreateDTO dto)
        {
            return Ok(await _taskService.CreateTask(dto));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TaskDTO dto)
        {
            await _taskService.UpdateTask(dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _taskService.DeleteTask(id);
            return NoContent();
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<TaskDTO> GetTask(int id)
        {
            var task = await _taskService.GetTask(id);

            return task;
        }

    }

   
}
