﻿using AutoMapper;
using EFCore.BLL.Services.Abstract;
using EFCore.Common.DTOs.Task;
using EFCore.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFCore.BLL.Services
{
    public class TaskService :BaseService
    {
        public TaskService(IMapper mapper, EFCoreDbContext context) : base(mapper, context) { }

        public async Task<ICollection<TaskDTO>> GetTasks()
        {

            var tasks = await _context.Tasks
               .Include(task => task.Performer)
               .Include(task=>task.Project)
               .ToListAsync();

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        public async Task<TaskDTO> CreateTask(TaskCreateDTO taskDTO)
        {
            var taskEntity = _mapper.Map<DAL.Entities.Task>(taskDTO);

            _context.Add(taskEntity);
            await _context.SaveChangesAsync();

            var createdTask = await _context.Tasks
               .FirstAsync(task => task.Id == taskEntity.Id);

            var createdTaskDTO = _mapper.Map<TaskDTO>(createdTask);

            return createdTaskDTO;
        }
        public async System.Threading.Tasks.Task UpdateTask(TaskDTO taskDTO)
        {
            var taskEntity = await GetTaskByIdInternal(taskDTO.Id);

            if (taskEntity != null)
            {
                taskEntity.Name = taskDTO.Name;
                taskEntity.Description = taskDTO.Description;

                _context.Tasks.Update(taskEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task DeleteTask(int id)
        {
            var taskEntity = await GetTaskByIdInternal(id);
            if (taskEntity != null)
            {
                _context.Tasks.Remove(taskEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<TaskDTO> GetTask(int id)
        {

            var taskEntity = await GetTaskByIdInternal(id);
            if (taskEntity != null)
            {
                return _mapper.Map<TaskDTO>(taskEntity);
            }
            else
            {
                return new TaskDTO();
            }
        }

        private async System.Threading.Tasks.Task<DAL.Entities.Task> GetTaskByIdInternal(int id)
        {
            return  await _context.Tasks
              .FirstOrDefaultAsync(task => task.Id == id);

        }
    }
}
