﻿using AutoMapper;
using EFCore.Common.DTOs.User;
using EFCore.BLL.Services.Abstract;
using EFCore.DAL.Context;
using EFCore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EFCore.Common.DTOs.Task;
using EFCore.Common.DTOs.Project;

namespace EFCore.BLL.Services
{
    public class UserService :BaseService
    {
        public UserService(IMapper mapper, EFCoreDbContext context) : base(mapper,context) {}

        ///<summary>Get list of projects where at least one task assigned to the particular used
        ///Returns Disctionary</summary>
        ///
        public async Task<Dictionary<ProjectDTO, int>> GetProjects(int id)
        {
            ProjectService ps = new ProjectService(_mapper,_context);
            var Projects = await ps.GetProjects();

            return Projects.Where(p => p.Tasks.Any(t => t.PerformerId == id)).ToDictionary(p => p, p => p.Tasks.Count);
        }

        ///<summary>Get a list of tasks assigned to the particular user and which name length less than 45 chars</summary>
        ///
        public async Task<List<TaskDTO>> GetTasks(int id)
        {
            var userEntity = await GetUserByIdInternal(id);
            var tasks = userEntity.Tasks.Where(t => t.Name.Length < 45);

            return _mapper.Map<List<TaskDTO>>(tasks);
        }

        ///<summary>Get a list of tasks finished in 2021 by particular user</summary>
        ///
        public async Task<List<Tuple<int,string>>> GetFinishedTasks(int id)
        {
            var userEntity = await GetUserByIdInternal(id);
            var tasks = userEntity.Tasks.Where(t => t.FinishedAt != null).Where(t => t.FinishedAt.Value.Year == 2021)
                .Select(t => new { t.Id, t.Name }).AsEnumerable()
                .Select(t => new Tuple<int, string>(t.Id, t.Name)).ToList();
            
            return tasks;
        }
        /// <summary>
        /// Returns Users with associated tasks
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserTasksDTO>> GetUsersWithTasks()
        {
            TaskService ts = new TaskService(_mapper, _context);

            var users = (from u in await GetUsers()
                         select new UserTasksDTO
                         {
                             FirstName = u.FirstName,
                             Tasks = (from t in u.Tasks
                                      where t.PerformerId == u.Id
                                      select (t)).OrderByDescending(t => t.Name.Length).ToList()
                         }).ToList();


            return users;
        }
        public async Task<UserDetailsDTO> GetUserDetails (int id)
        {

            ProjectService ps = new ProjectService(_mapper, _context);
            var Projects = await ps.GetProjects();

            var userDTO = (from u in await GetUsers()
                        let projects= ( from p in Projects
                                       where p.Tasks.Any(t => t.PerformerId == u.Id)
                                       select (p))
                        where u.Id == id
                        select new UserDetailsDTO
                        {
                            User = u,
                            Project = projects.OrderByDescending(x => x.CreatedAt).FirstOrDefault(),

                            NumberOfTasks = projects.OrderByDescending(x => x.CreatedAt).FirstOrDefault()?.Tasks.Count,

                            UnfinishedTasks = (from t in u.Tasks
                                               where t.PerformerId == u.Id && t.FinishedAt == null
                                               select (t)).Count(),

                            LongestTask = (from t in u.Tasks
                                           orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt descending
                                           where t.PerformerId == u.Id
                                           select (t)).FirstOrDefault()
                        }).FirstOrDefault();

            return userDTO;

        }

        public async Task<ICollection<UserDTO>> GetUsers()
        {

            var users = await _context.Users
               .Include(user => user.Team)
               .Include(user=>user.Tasks)
               .Include(user=>user.Projects)
               .ToListAsync();

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> CreateUser(UserCreateDTO userDTO)
        {
            var userEntity = _mapper.Map<User>(userDTO);
            userEntity.RegisteredAt = System.DateTime.Now;

            _context.Add(userEntity);
            await _context.SaveChangesAsync();

            var createdUser = await _context.Users
               .FirstAsync(user => user.Id == userEntity.Id);

            var createdTeamDTO = _mapper.Map<UserDTO>(createdUser);

            return createdTeamDTO;
        }
        public async System.Threading.Tasks.Task UpdateUser(UserDTO userDTO)
        {
            var userEntity = await GetUserByIdInternal(userDTO.Id);

            if (userEntity != null)
            {
                userEntity.TeamId = userDTO.TeamId;
                userEntity.FirstName = userDTO.FirstName;
                userEntity.LastName = userDTO.LastName;
                userEntity.Email = userDTO.Email;
                userEntity.Birthday = userDTO.Birthday;

                _context.Users.Update(userEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            var userEntity = await GetUserByIdInternal(id);
            if (userEntity != null)
            {
                _context.Users.Remove(userEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task<UserDTO> GetUser(int id)
        {
            var userEntity = await GetUserByIdInternal(id);
            if (userEntity != null)
            {
                return _mapper.Map<UserDTO>(userEntity);
            }
            else
            {
                return new UserDTO();
            }
        }


        private async Task<User> GetUserByIdInternal(int id)
        {
            return await _context.Users
               .Include(user => user.Team)
               .Include(user => user.Tasks)
               .Include(user => user.Projects)
              .FirstOrDefaultAsync(user => user.Id == id);

        }
    }
}
