﻿using AutoMapper;
using EFCore.DAL.Context;

namespace EFCore.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;
        private protected readonly EFCoreDbContext _context;

        public BaseService(IMapper mapper,EFCoreDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }
    }
}
