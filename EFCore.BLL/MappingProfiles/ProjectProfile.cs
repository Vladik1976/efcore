﻿using AutoMapper;
using EFCore.Common.DTOs.Project;
using EFCore.DAL.Entities;
using System.Linq;

namespace EFCore.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>();

            CreateMap<ProjectCreateDTO, Project>();

            CreateMap<Project, ProjectCreateDTO>();

            CreateMap<Project, ProjectDetailsDTO>()
                .ForMember(dest => dest.LongestTask, src => src.MapFrom(s => s.Tasks.OrderByDescending(x => x.Description).FirstOrDefault()))
                .ForMember(dest => dest.ShortestTask, src => src.MapFrom(s => s.Tasks.OrderBy(x => x.Name).FirstOrDefault()))
                .ForMember(dest => dest.NumberOfPerformers, src => src.MapFrom(
                     s => (from t in s.Tasks
                           where s.Description.Length > 20 || s.Tasks.Count < 3
                           select t.PerformerId).Distinct().Count()));






        }
    }
}
