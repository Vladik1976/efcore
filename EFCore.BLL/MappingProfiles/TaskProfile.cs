﻿using AutoMapper;
using EFCore.Common.DTOs.Task;
using EFCore.DAL.Entities;

namespace EFCore.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();

            CreateMap<TaskDTO, Task>();

            CreateMap<Task, TaskCreateDTO>();

            CreateMap<TaskCreateDTO, Task>();

        }
    }
}
