﻿using AutoMapper;
using EFCore.Common.DTOs.Team;
using EFCore.DAL.Entities;

namespace EFCore.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>();

            CreateMap<TeamCreateDTO, Team>();

            CreateMap<Team, TeamCreateDTO>();
        }
    }
}