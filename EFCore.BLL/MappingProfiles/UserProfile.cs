﻿
using AutoMapper;
using EFCore.Common.DTOs.User;
using EFCore.DAL.Entities;

namespace EFCore.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();

            CreateMap<UserDTO, User>();

            CreateMap<UserCreateDTO, User>();

            CreateMap<User, UserCreateDTO>();

        }
    }
}
