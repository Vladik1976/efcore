﻿using EFCore.Common.DTOs.Task;
using EFCore.Common.DTOs.Team;
using EFCore.Common.DTOs.User;
using System;
using System.Collections.Generic;

namespace EFCore.Common.DTOs.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }

        public UserDTO Author { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int TeamId { get; set; }

        public TeamDTO Team { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? Deadline { get; set; }

        public ICollection<TaskDTO> Tasks { get; set; }
    }
}
