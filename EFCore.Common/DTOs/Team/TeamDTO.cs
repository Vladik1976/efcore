﻿
using EFCore.Common.DTOs.User;
using System.Collections.Generic;

namespace EFCore.Common.DTOs.Team
{
    public class TeamDTO
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public ICollection<UserDTO> Users { get; set; }
    }
}
