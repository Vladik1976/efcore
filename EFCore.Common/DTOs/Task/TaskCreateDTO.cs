﻿using System;

namespace EFCore.Common.DTOs.Task
{
    public class TaskCreateDTO
    {
        public int ProjectId { get; set; }
  
        public int PerformerId { get; set; }


        public string Name { get; set; }

        public string Description { get; set; }

        public int State { get; set; }

        public DateTime CreatedAt { get; set; }

    }
}
