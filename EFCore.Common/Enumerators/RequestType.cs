﻿namespace EFCore.Common.Enumerators
{
    public enum RequestType
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
