﻿using Newtonsoft.Json;
using EFCore.Common.DTOs.Project;
using EFCore.Common.DTOs.Task;
using EFCore.Common.DTOs.User;
using EFCore.Client.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using EFCore.Common.Enumerators;
using EFCore.Common.DTOs.Team;

namespace EFCore.Client
{
    class Program
    {
        private static HttpService _client;

        static void Main(string[] args)
        {
            _client = new HttpService(ConfigurationManager.AppSettings["EndPointURL"].ToString());

            BasicMenu();
        }

        private static async Task ProjectsByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetProjects/" + Id.ToString());
                Dictionary<ProjectDTO, int> projects = JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(response);

                foreach (KeyValuePair<ProjectDTO, int> keyValue in projects)
                {
                    Console.WriteLine($"Project {keyValue.Key.Name}  Tasks:{keyValue.Value.ToString()}");
                }

                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static async Task TasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetTasks/" + Id.ToString());
                List<TaskDTO> tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(response);

                foreach (TaskDTO t in tasks)
                {
                    Console.WriteLine($"Task Name:{t.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static async Task FinishedTasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetFinishedTasks/" + Id.ToString());

                List<Tuple<int, string>> tasks = JsonConvert.DeserializeObject<List<Tuple<int, string>>>(response);

                foreach (Tuple<int, string> t in tasks)
                {
                    Console.WriteLine($"Task Id:  {t.Item1} Task Name:{t.Item2}");
                }

                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static async Task GetTeamsLimited()
        {
            string response = await _client.GetStringAsync("/teams/GetTeamsLimited/");

            List<Tuple<int, string, List<UserDTO>>> teams = JsonConvert.DeserializeObject<List<Tuple<int, string, List<UserDTO>>>>(response);

            foreach (Tuple<int, string, List<UserDTO>> x in teams)
            {
                Console.WriteLine($"TeamID:{x.Item1} Name:{x.Item2} Users:");
                foreach (UserDTO z in x.Item3)
                {
                    Console.WriteLine($"First Name:{z.FirstName} Last Name:{z.LastName}");
                }
            }
            BasicMenu();
        }

        private static async Task GetUsersWithTasks()
        {
            string response = await _client.GetStringAsync("/users/GetUsersWithTasks/");

            List<Tuple<string, List<TaskDTO>>> users = JsonConvert.DeserializeObject<List<Tuple<string, List<TaskDTO>>>>(response);

            foreach (Tuple<string, List<TaskDTO>> x in users)
            {
                Console.WriteLine($"First Name:{x.Item1} Tasks:");
                foreach (TaskDTO z in x.Item2)
                {
                    Console.WriteLine($"Name:{z.Name}");
                }

            }
            BasicMenu();
        }

        private static void IncorectInput()
        {
            Console.WriteLine("Incorrect input");
            BasicMenu();
        }

        private static async Task GetUserDetails()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                string response = await _client.GetStringAsync("/users/GetUserDetails/" + Id);

                UserDetailsDTO userDetails = JsonConvert.DeserializeObject<UserDetailsDTO>(response);

                if (userDetails.User == null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {

                    Console.WriteLine($"First Name:{userDetails.User.FirstName}");
                    Console.WriteLine($"Last Project:{userDetails.Project.Name}");
                    Console.WriteLine($"Number of Tasks in a Last Project:{userDetails.Project.Tasks.Count}");
                    Console.WriteLine($"Number of Finished Tasks :{userDetails.Project.Tasks.Count}");
                    Console.WriteLine($"Longest Task:{userDetails.LongestTask.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static async Task GetProjectDetails()
        {
            Console.WriteLine("Project Id");
            var ProjectId = Console.ReadLine();

            if (int.TryParse(ProjectId, out int Id))
            {
                string response = await _client.GetStringAsync("/projects/GetProjectDetails/" + Id);

                ProjectDetailsDTO projectDetails = JsonConvert.DeserializeObject<ProjectDetailsDTO>(response);

                if (projectDetails.Project == null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {
                    Console.WriteLine($"Project Name: {projectDetails.Project.Name}");
                    Console.WriteLine($"Longest Task: {projectDetails.LongestTask.Name}");
                    Console.WriteLine($"Shortest Task: {projectDetails.ShortestTask.Name}");
                    Console.WriteLine($"Users involved: {projectDetails.NumberOfPerformers}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static async Task AddNewUser()
        {
            Console.WriteLine("First Name");
            var firstName = Console.ReadLine();
            Console.WriteLine("Last Name");
            var lastName = Console.ReadLine();
            Console.WriteLine("Email");
            var email = Console.ReadLine();

            var userData = new UserCreateDTO()
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };
            var response = await _client.SendRequest("/users/Create/", userData, RequestType.POST);
            UserDTO userDetails = JsonConvert.DeserializeObject<UserDTO>(response);
            Console.WriteLine($"New User {userDetails.FirstName} was successfully added.");
            BasicMenu();
        }

        private static async Task EditUser()
        {
            Console.WriteLine("User ID");
            var userId = Console.ReadLine();
            Console.WriteLine("First Name");
            var firstName = Console.ReadLine();
            Console.WriteLine("Last Name");
            var lastName = Console.ReadLine();
            Console.WriteLine("Email");
            var email = Console.ReadLine();

            if (int.TryParse(userId, out int id))
            {
                var userData = new UserDTO()
                {
                    Id = id,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email
                };
                var response = await _client.SendRequest("/users/Update/", userData, RequestType.PUT);
                Console.WriteLine($"User has successfully updated.");
            }
            else
            {
                Console.WriteLine("Wrong User ID");
            }

            BasicMenu();
        }
        public static async Task DeleteUser()
        {
            Console.WriteLine("User ID");
            var userId = Console.ReadLine();
            if (int.TryParse(userId, out int id))
            {
                var response = await _client.SendRequest($"/users/Delete/{id}", null, RequestType.DELETE);
                Console.WriteLine($"User has successfully deleted.");
            }
            else
            {
                Console.WriteLine("Wrong User ID");
            }
            BasicMenu();
        }
        public static async Task GetUser()
        {
            Console.WriteLine("User ID");
            var userId = Console.ReadLine();
            if (int.TryParse(userId, out int id))
            {
                var response = await _client.SendRequest($"/users/GetUser/{id}", null, RequestType.GET);
                UserDTO userDetails = JsonConvert.DeserializeObject<UserDTO>(response);
                if (userDetails != null)
                {
                    Console.WriteLine($"First Name {userDetails.FirstName}");
                    Console.WriteLine($"Last Name {userDetails.LastName}");
                    Console.WriteLine($"Email {userDetails.Email}");
                }
                else
                {
                    Console.WriteLine("Not Found");
                }
            }
            else
            {
                Console.WriteLine("Wrong User ID");
            }
            BasicMenu();
        }

        private static async Task AddNewProject()
        {
            Console.WriteLine("Project Name");
            var name = Console.ReadLine();
            Console.WriteLine("Project Description");
            var description = Console.ReadLine();

            var projectData = new ProjectCreateDTO()
            {
                Name = name,
                Description = description,
                AuthorId = 1,
                TeamId = 1
            };
            var response = await _client.SendRequest("/projects/Create/", projectData, RequestType.POST);
            ProjectDTO projectDetails = JsonConvert.DeserializeObject<ProjectDTO>(response);
            Console.WriteLine($"New Project {projectDetails.Name} was successfully added.");
            BasicMenu();
        }
        private static async Task EditProject()
        {
            Console.WriteLine("Project ID");
            var projectId = Console.ReadLine();
            Console.WriteLine("Project Name");
            var name = Console.ReadLine();
            Console.WriteLine("Project Description");
            var description = Console.ReadLine();

            if (int.TryParse(projectId, out int id))
            {
                var projectData = new ProjectDTO()
                {
                    Id = id,
                    Name = name,
                    Description = description,
                };
                var response = await _client.SendRequest("/projects/Update/", projectData, RequestType.PUT);
                Console.WriteLine($"Project has successfully updated.");
            }
            else
            {
                Console.WriteLine("Wrong Project ID");
            }

            BasicMenu();
        }
        public static async Task DeleteProject()
        {
            Console.WriteLine("Project ID");
            var projectId = Console.ReadLine();
            if (int.TryParse(projectId, out int id))
            {
                var response = await _client.SendRequest($"/projects/Delete/{id}", null, RequestType.DELETE);
                Console.WriteLine($"Paroject has successfully deleted.");
            }
            else
            {
                Console.WriteLine("Wrong Project ID");
            }
            BasicMenu();
        }
        public static async Task GetProject()
        {
            Console.WriteLine("Project ID");
            var projectId = Console.ReadLine();
            if (int.TryParse(projectId, out int id))
            {
                var response = await _client.SendRequest($"/projects/GetProject/{id}", null, RequestType.GET);
                ProjectDTO projectDetails = JsonConvert.DeserializeObject<ProjectDTO>(response);
                if (projectDetails != null)
                {
                    Console.WriteLine($"Name {projectDetails.Name}");
                    Console.WriteLine($"Description {projectDetails.Description}");
                }
                else
                {
                    Console.WriteLine("Not Found");
                }
            }
            else
            {
                Console.WriteLine("Wrong Project ID");
            }
            BasicMenu();
        }

        private static async Task AddNewTeam()
        {
            Console.WriteLine("Team Name");
            var name = Console.ReadLine();

            var teamData = new TeamCreateDTO()
            {
                Name = name,
            };
            var response = await _client.SendRequest("/teams/Create/", teamData, RequestType.POST);
            TeamDTO teamDetails = JsonConvert.DeserializeObject<TeamDTO>(response);
            Console.WriteLine($"New Team {teamDetails.Name} was successfully added.");
            BasicMenu();
        }
        private static async Task EditTeam()
        {
            Console.WriteLine("Team ID");
            var teamId = Console.ReadLine();
            Console.WriteLine("Team Name");
            var name = Console.ReadLine();

            if (int.TryParse(teamId, out int id))
            {
                var teamData = new TeamDTO()
                {
                    Id = id,
                    Name = name
                };
                var response = await _client.SendRequest("/teams/Update/", teamData, RequestType.PUT);
                Console.WriteLine($"Team has successfully updated.");
            }
            else
            {
                Console.WriteLine("Wrong Team ID");
            }

            BasicMenu();
        }
        public static async Task DeleteTeam()
        {
            Console.WriteLine("Team ID");
            var teamId = Console.ReadLine();
            if (int.TryParse(teamId, out int id))
            {
                var response = await _client.SendRequest($"/teams/Delete/{id}", null, RequestType.DELETE);
                Console.WriteLine($"Team has successfully deleted.");
            }
            else
            {
                Console.WriteLine("Wrong Team ID");
            }
            BasicMenu();
        }
        public static async Task GetTeam()
        {
            Console.WriteLine("Team ID");
            var teamId = Console.ReadLine();
            if (int.TryParse(teamId, out int id))
            {
                var response = await _client.SendRequest($"/teams/GetTeam/{id}", null, RequestType.GET);
                TeamDTO teamDetails = JsonConvert.DeserializeObject<TeamDTO>(response);
                if (teamDetails != null)
                {
                    Console.WriteLine($"Name {teamDetails.Name}");
                }
                else
                {
                    Console.WriteLine("Not Found");
                }
            }
            else
            {
                Console.WriteLine("Wrong Team ID");
            }
            BasicMenu();
        }
        private static async Task AddNewTask()
        {
            Console.WriteLine("Task Name");
            var name = Console.ReadLine();
            Console.WriteLine("Task Description");
            var description = Console.ReadLine();

            var taskData = new TaskCreateDTO()
            {
                Name = name,
                Description=description,
                ProjectId=1,
                PerformerId=1,
                State=0
            };
            var response = await _client.SendRequest("/tasks/Create/", taskData, RequestType.POST);
            TaskDTO taskDetails = JsonConvert.DeserializeObject<TaskDTO>(response);
            Console.WriteLine($"New Task {taskDetails.Name} has successfully added.");
            BasicMenu();
        }
        private static async Task EditTask()
        {
            Console.WriteLine("Task ID");
            var taskId = Console.ReadLine();
            Console.WriteLine("Task Name");
            var name = Console.ReadLine();
            Console.WriteLine("Task Name");
            var description = Console.ReadLine();

            if (int.TryParse(taskId, out int id))
            {
                var taskData = new TaskDTO()
                {
                    Id = id,
                    Name = name,
                    Description=description
                };
                var response = await _client.SendRequest("/tasks/Update/", taskData, RequestType.PUT);
                Console.WriteLine($"Task has successfully updated.");
            }
            else
            {
                Console.WriteLine("Wrong Task ID");
            }

            BasicMenu();
        }
        public static async Task DeleteTask()
        {
            Console.WriteLine("Task ID");
            var taskId = Console.ReadLine();
            if (int.TryParse(taskId, out int id))
            {
                var response = await _client.SendRequest($"/tasks/Delete/{id}", null, RequestType.DELETE);
                Console.WriteLine($"Task has successfully deleted.");
            }
            else
            {
                Console.WriteLine("Wrong Task ID");
            }
            BasicMenu();
        }
        public static async Task GetTask()
        {
            Console.WriteLine("Task ID");
            var teamId = Console.ReadLine();
            if (int.TryParse(teamId, out int id))
            {
                var response = await _client.SendRequest($"/tasks/GetTask/{id}", null, RequestType.GET);
                TaskDTO taskDetails = JsonConvert.DeserializeObject<TaskDTO>(response);
                if (taskDetails != null)
                {
                    Console.WriteLine($"Name {taskDetails.Name}");
                    Console.WriteLine($"Description {taskDetails.Description}");
                }
                else
                {
                    Console.WriteLine("Not Found");
                }
            }
            else
            {
                Console.WriteLine("Wrong Task ID");
            }
            BasicMenu();
        }
        public static void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("0 - Number of tasks in a project for a particular user.");
            Console.WriteLine("1 - List of tasks for a particular user.");
            Console.WriteLine("2 - Get list of tasks finished in 2021 by particular user.");
            Console.WriteLine("3 - Get list of temas contains users over 10 years old.");
            Console.WriteLine("4 - Get list of users with tasks assigned");
            Console.WriteLine("5 - Get User details");
            Console.WriteLine("6 - Get Project Details");
            Console.WriteLine("7 - Add New User");
            Console.WriteLine("8 - Edit User");
            Console.WriteLine("9 - Delete User");
            Console.WriteLine("q - Get User");
            Console.WriteLine("w - Add New Project");
            Console.WriteLine("e - Edit Project");
            Console.WriteLine("r - Delete Project");
            Console.WriteLine("t - Get Project");
            Console.WriteLine("y - Add New Team");
            Console.WriteLine("u - Edit Team");
            Console.WriteLine("i - Delete Team");
            Console.WriteLine("o - Get Team");
            Console.WriteLine("p - Add New Task");
            Console.WriteLine("a - Edit Task");
            Console.WriteLine("s - Delete Task");
            Console.WriteLine("d - Get Task");

            var input = Console.ReadKey();
            switch (input.KeyChar)
            {
                case '0':
                    ProjectsByUser().Wait();
                    break;
                case '1':
                    TasksByUser().Wait();
                    break;
                case '2':
                    FinishedTasksByUser().Wait();
                    break;
                case '3':
                    GetTeamsLimited().Wait();
                    break;
                case '4':
                    GetUsersWithTasks().Wait();
                    break;
                case '5':
                    GetUserDetails().Wait();
                    break;
                case '6':
                    GetProjectDetails().Wait();
                    break;
                case '7':
                    AddNewUser().Wait();
                    break;
                case '8':
                    EditUser().Wait();
                    break;
                case '9':
                    DeleteUser().Wait();
                    break;
                case 'q':
                    GetUser().Wait();
                    break;
                case 'w':
                    AddNewProject().Wait();
                    break;
                case 'e':
                    EditProject().Wait();
                    break;
                case 'r':
                    DeleteProject().Wait();
                    break;
                case 't':
                    GetProject().Wait();
                    break;
                case 'y':
                    AddNewTeam().Wait();
                    break;
                case 'u':
                    EditTeam().Wait();
                    break;
                case 'i':
                    DeleteTeam().Wait();
                    break;
                case 'o':
                    GetTeam().Wait();
                    break;
                case 'p':
                    AddNewTask().Wait();
                    break;
                case 'a':
                    EditTask().Wait();
                    break;
                case 's':
                    DeleteTask().Wait();
                    break;
                case 'd':
                    GetTask().Wait();
                    break;
                default:
                    IncorectInput();
                    break;
            }
        }

    }
}
