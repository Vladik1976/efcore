﻿using EFCore.Common.Enumerators;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Client.Services
{
    public class HttpService : IDisposable
    {
        private readonly HttpClient _client;
        private readonly string _endPointURL;

        public HttpService(string endPointURL)
        {
            _client = new HttpClient();
            _endPointURL = endPointURL;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<string> GetStringAsync(string param)
        {
            return await _client.GetStringAsync(_endPointURL + param);
        }

        public async Task<string> SendRequest(string route, object dataDTO, RequestType requestType)
        {
            string json = JsonConvert.SerializeObject(dataDTO);
            StringContent data = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response;
            switch (requestType)
            {
                case RequestType.POST:
                    response = await _client.PostAsync(_endPointURL + route, data);
                    break;
                case RequestType.PUT:
                    response = await _client.PutAsync(_endPointURL + route, data);
                    break;
                case RequestType.DELETE:
                    response = await _client.DeleteAsync(_endPointURL + route);
                    break;
                case RequestType.GET:
                    response = await _client.GetAsync(_endPointURL+ route);
                    break;
                default:
                        response = await _client.PostAsync(_endPointURL + route, data);
                    break;
            }

            string result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }

  }
