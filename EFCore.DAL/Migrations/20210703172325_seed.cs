﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCore.DAL.Migrations
{
    public partial class seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Projects",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Projects",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 7, 3, 20, 23, 25, 276, DateTimeKind.Local).AddTicks(4609), "Clever team" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(2011, 4, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(945), "billy@microsoft.com", "Bill", "Gates", new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(937), null });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 4, new DateTime(2011, 7, 10, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(978), "haha@cake.com", "Lazy", "Cat", new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(975), null });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(2777), new DateTime(2021, 7, 11, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(3031), "Good Description for the first project", "My first Project", 1 },
                    { 2, 4, new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(3328), new DateTime(2021, 7, 8, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(3336), "and nothing more...", "a botle of Milk", null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2011, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(576), "johndoe@gmail.com", "John", "Doe", new DateTime(2021, 7, 3, 20, 23, 25, 278, DateTimeKind.Local).AddTicks(9801), 1 },
                    { 3, new DateTime(2011, 3, 8, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(961), "ent@brown.com", "Entony", "Brown", new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(958), 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2021, 7, 3, 20, 23, 25, 279, DateTimeKind.Local).AddTicks(4730), "you must do something", null, "Nice task for the First Project", 3, 1, 0 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 2, null, "No comments...", null, "Just dummy task for the seeding in my HomeWork", 4, 2, 0 });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Projects",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Projects",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
