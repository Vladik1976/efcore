﻿using EFCore.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore.DAL.Entities
{
    public class Team:BaseEntity
    {
        public Team()
        {
            Users = new List<User>();
        }

        [Required]
        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<User> Users { get; set; }

    }
}
