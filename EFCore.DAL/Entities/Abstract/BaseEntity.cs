﻿namespace EFCore.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {

        public BaseEntity() { }
        

        public int Id { get; set; }

    }
}
